import { useParams } from 'react-router-dom';
import { useState, useEffect } from 'react';
import axios from 'axios';
import { BannerPersonaje } from '../Componentes/Personajes/BannerPersonajes';
import { ListaCapitulos } from '../Componentes/Capitulos/ListadoCapitulos';

//TODO Search
export function Personajes() {
  let { id } = useParams();
  let [personaje, setPersonaje] = useState(null);
  let [capitulos, setCapitulos] = useState();

  //Se obtinenen los personajes
  useEffect(() => {
    axios
      .get(`https://rickandmortyapi.com/api/character/${id}`)
      .then((respuesta) => {
        setPersonaje(respuesta.data);
      });
  }, []);

  //Se obtienen los capitulos
  useEffect(() => {
    if (personaje) {
      let listaCapitulos = personaje.episode.map((capitulo) => {
        return axios.get(capitulo);
      });

      Promise.all(listaCapitulos).then((respuestas) => {
        setCapitulos(respuestas);
      });
    }
  }, [personaje]);

  return (
    <div className="col-12 p-5">
      {personaje ? (
        <div>
          <BannerPersonaje {...personaje} />

          <h2 className="py-4">Capitulos</h2>

          {capitulos ? (
            <ListaCapitulos capitulos={capitulos} />
          ) : (
            <div>Cargando...</div>
          )}
        </div>
      ) : (
        <div>Cargando...</div>
      )}
    </div>
  );
}
