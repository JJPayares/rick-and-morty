import { Banner } from '../Componentes/Navbar/Banner';
import { Buscador } from '../Componentes/Buscador/Buscador';
import { ListadoPersonajes } from '../Componentes/Personajes/ListadoPersonajes';
import { useState } from 'react';
export function Home() {
  let [buscador, setBuscador] = useState('');
  return (
    <div className="">
      <Banner />
      <div className="container ">
        <h1 className="py-4">Personajes populares</h1>
      </div>
      <Buscador valor={buscador} onBuscar={setBuscador} />

      <ListadoPersonajes buscar={buscador} />
    </div>
  );
}
