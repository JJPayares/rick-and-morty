import bootstrap from 'bootstrap/dist/css/bootstrap.min.css';
import { useState, useEffect } from 'react';
import { Routes, Route, Link } from 'react-router-dom';
import { Home } from './Pages/Home';
import { Personajes } from './Pages/Personajes';
import { Header } from './Componentes/Navbar/Header';
import { Footer } from './Componentes/Footer/Footer';
function App() {
  return (
    <div className="App">
      <Header />

      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/personaje/:id" element={<Personajes />} />
      </Routes>

      <Footer />
    </div>
  );
}

export default App;
