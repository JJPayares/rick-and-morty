export function Footer() {
  return (
    <div className="navbar-ligth bg-light fixed-bottom mb-0">
      <p className="text-center">
        Proyecto creado por Juan Payares para la clase de ReactJS Basico - 2022
      </p>
    </div>
  );
}
