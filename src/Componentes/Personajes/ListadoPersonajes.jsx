import { Personaje } from './Personaje';
import { useEffect, useState } from 'react';
import axios from 'axios';

export function ListadoPersonajes({ buscar }) {
  const [personajes, setPersonajes] = useState(null);
  let personajeBuscado = personajes?.results;

  if (buscar && personajes) {
    personajeBuscado = personajes.results.filter((personaje) => {
      let nombreMinuscula = personaje.name.toLowerCase();
      let buscadoMinuscula = buscar.toLowerCase();
      return nombreMinuscula.includes(buscadoMinuscula);
    });
  }

  useEffect(() => {
    axios.get('https://rickandmortyapi.com/api/character').then((respuesta) => {
      setPersonajes(respuesta.data);
    });
  }, []);

  return (
    <div className="row py-5">
      {personajeBuscado
        ? personajeBuscado.map((personaje) => {
            return <Personaje key={personaje.id} {...personaje} />;
          })
        : 'Cargando...'}
    </div>
  );
}
