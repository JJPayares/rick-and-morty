import { Capitulo } from './Capitulo';

export function ListaCapitulos({ capitulos }) {
  return (
    <div className="row">
      {capitulos.map((capitulo) => (
        <Capitulo key={capitulo.data.id} {...capitulo.data} />
      ))}
    </div>
  );
}
