import bannerImg from '../../assets/Imgs/banner-img.jpeg';
export function Banner() {
  return (
    <div className="">
      <img
        style={{ heigth: '600px', objectFit: 'cover' }}
        src={bannerImg}
        class="card-img"
        alt="banner-home"
      />
    </div>
  );
}
