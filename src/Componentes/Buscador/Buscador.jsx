export function Buscador({ valor, onBuscar }) {
  return (
    <div className="d-flex justify-content-end">
      <div className="col-5 mb-3">
        <input
          type="text"
          className="form-control"
          placeholder="Busca tu personaje."
          onChange={(evento) => onBuscar(evento.target.value)}
          value={valor}
        />
      </div>
    </div>
  );
}
